package com.carona.backendCarona;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendCaronaApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendCaronaApplication.class, args);
	}


}
