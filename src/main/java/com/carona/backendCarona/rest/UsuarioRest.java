package com.carona.backendCarona.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carona.backendCarona.dao.UsuarioDao;
import com.carona.backendCarona.entity.Usuario;

@SpringBootApplication
@RestController
@RequestMapping("/usuarios")
public class UsuarioRest {
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@GetMapping
	public List<Usuario> get(){
		return usuarioDao.findAll();
	}
	
	@GetMapping("/{cpf}")
	public Usuario getByCpf(@PathVariable String cpf){
		return usuarioDao.findByCpf(cpf);
	}
	
	@GetMapping("/existe/{cpf}")
	public boolean verificarCpfIgual(@PathVariable String cpf) {
		return usuarioDao.existsByCpf(cpf);
	}
	
	@GetMapping("/autenticar/{cpf}/{senha}")
	public boolean autenticar(@PathVariable String cpf, @PathVariable String senha) {
		return usuarioDao.existsByCpfAndSenha(cpf, senha);
	}

	@PostMapping
	public void post(@RequestBody Usuario usuario) {
		usuarioDao.save(usuario);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id){
		usuarioDao.deleteById(id);
	}

}
