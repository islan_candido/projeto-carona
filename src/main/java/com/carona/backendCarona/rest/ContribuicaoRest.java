package com.carona.backendCarona.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carona.backendCarona.dao.ContribuicaoDao;
import com.carona.backendCarona.entity.Contribuicao;

@RestController
@RequestMapping("/contribuições")
public class ContribuicaoRest {
	
	@Autowired
	private ContribuicaoDao contribuicaoDao;
	
	@GetMapping
	public List<Contribuicao> get(){
		return contribuicaoDao.findAll();
	}
	
	@GetMapping("/{id}")
	public Object getById(@PathVariable Long id){
		return contribuicaoDao.findById(id);
	}
	
	@PostMapping
	public void post(@RequestBody Contribuicao contribuicao) {
		contribuicaoDao.save(contribuicao);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id){
		contribuicaoDao.deleteById(id);
	}
}
