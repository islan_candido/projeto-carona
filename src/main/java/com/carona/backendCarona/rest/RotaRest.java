package com.carona.backendCarona.rest;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carona.backendCarona.dao.RotaDao;
import com.carona.backendCarona.entity.Rota;

@RestController
@RequestMapping("/rotas")
public class RotaRest {

	@Autowired
	private RotaDao rotaDao;
	
	@GetMapping
	public List<Rota> get(){
		return rotaDao.findAll();
	}
	
	@GetMapping("/{verificador}")
	public Rota getByVerificador(@PathVariable String verificador){
		return rotaDao.findByVerificador(verificador);
	}
	
	@GetMapping("/consulta/{status}/{fim}/{data}")
	public List<Rota> getRotaByFim(@PathVariable String status, @PathVariable String fim, @PathVariable Date data){
		Calendar c = Calendar.getInstance();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		
		return rotaDao.findByStatusAndFimContainsIgnoreCaseAndDataBetweenOrderByData(status,fim, c, cal);
	}
	
	@GetMapping("/filter/disponiveis/{status}/{data}")
	public List<Rota> getRotasDisponiveis(@PathVariable String status, @PathVariable Date data){
		
		Calendar c = Calendar.getInstance();
		
		Calendar cal = Calendar.getInstance();
		cal.setTime(data);
		
		return rotaDao.findByStatusAndDataBetweenOrderByData(status, c, cal);
	}

	
	@GetMapping("/existe/{verificador}")
	public boolean verificarCodigoIgual(@PathVariable String verificador) {
		return rotaDao.existsByVerificador(verificador);
	}
	
	@PostMapping
	public void post(@RequestBody Rota rota) {
		rotaDao.save(rota);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id){
		rotaDao.deleteById(id);
	}
	
}
