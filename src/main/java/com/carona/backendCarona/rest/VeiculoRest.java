package com.carona.backendCarona.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carona.backendCarona.dao.VeiculoDao;
import com.carona.backendCarona.entity.Veiculo;

@RestController
@RequestMapping("/veiculos")
public class VeiculoRest {

	@Autowired
	private VeiculoDao veiculoDao;
	
	@GetMapping
	public List<Veiculo> get(){
		return veiculoDao.findAll();
	}
	
	@GetMapping("/{placa}")
	public Veiculo getByPlaca(@PathVariable String placa){
		return veiculoDao.findByPlaca(placa);
	}
	
	@GetMapping("/existe/{renavam}")
	public boolean verificarRenavamIgual(@PathVariable String renavam) {
		return veiculoDao.existsByRenavam(renavam);
	}
	
	@GetMapping("/verificar/existe/{placa}")
	public boolean verificarPlacaIgual(@PathVariable String placa) {
		return veiculoDao.existsByPlaca(placa);
	}

	@PostMapping
	public void post(@RequestBody Veiculo veiculo) {
		veiculoDao.save(veiculo);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id){
		veiculoDao.deleteById(id);
	}
}
