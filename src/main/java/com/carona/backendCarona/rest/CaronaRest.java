package com.carona.backendCarona.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carona.backendCarona.dao.CaronaDao;
import com.carona.backendCarona.entity.Carona;

@RestController
@RequestMapping("/caronas")
public class CaronaRest {
	
	@Autowired
	private CaronaDao caronaDao;

	@GetMapping
	public List<Carona> get(){
		return caronaDao.findAll();
	}
	
	@GetMapping("/{id}")
	public Object getById(@PathVariable Long id){
		return caronaDao.findById(id);
	}
	
	@GetMapping("/filter/{verificador}/{cpf}")
	public Carona getByVerificadorAndCpf(@PathVariable String verificador, @PathVariable String cpf) {
		return caronaDao.findByRotaVerificadorAndUsuarioCpf(verificador, cpf);
	}
	
	@GetMapping("/existe/{verificador}/{cpf}")
	public boolean existeVerificadorAndCpf(@PathVariable String verificador, @PathVariable String cpf) {
		return caronaDao.existsByRotaVerificadorAndUsuarioCpf(verificador, cpf);
	}
	
	@GetMapping("/consulta/{cpf}/{situacao}")
	public List<Carona> getBySituacaoAndUsuario(@PathVariable String cpf, @PathVariable String situacao){
		return caronaDao.findByRotaVeiculoUsuarioCpfAndSituacao(cpf, situacao);
	}

	@PostMapping
	public void post(@RequestBody Carona carona) {
		caronaDao.save(carona);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id){
		caronaDao.deleteById(id);
	}
}
