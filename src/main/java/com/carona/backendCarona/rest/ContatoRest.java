package com.carona.backendCarona.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.carona.backendCarona.dao.ContatoDao;
import com.carona.backendCarona.entity.Contato;

@RestController
@RequestMapping("/contatos")
public class ContatoRest {

	@Autowired
	private ContatoDao contatoDao;
	
	@GetMapping
	public List<Contato> get(){
		return contatoDao.findAll();
	}
	
	@GetMapping("/{id}")
	public Object getById(@PathVariable Long id){
		return contatoDao.findById(id);
	}
	
	@GetMapping("/filter/{cpf}")
	public List<Contato> listarContatos(@PathVariable String cpf){
		return contatoDao.findByUsuarioCpf(cpf); 
	}

	@PostMapping
	public void post(@RequestBody Contato contato) {
		contatoDao.save(contato);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable Long id){
		contatoDao.deleteById(id);
	}
}
