package com.carona.backendCarona.entity;

import java.sql.Time;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Rota {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="rot_id") 
	private Long id;
	
	@Column(name="rot_data") 
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Calendar data;
	
	@Column(name="rot_horario") 
	private Time horario;
	
	@Column(name="rot_inicio") 
	private String inicio;
	
	@Column(name="rot_fim") 
	private String fim;
	
	@Column(name="rot_status") 
	private String status;
	
	@Column(name="rot_cod_verificador") 
	private String verificador;
	
	@ManyToOne
	@JoinColumn(name="rot_fk_vei") 
	private Veiculo veiculo;
	
	@ManyToOne
	@JoinColumn(name="rot_fk_cont") 
	private Contribuicao contribuicao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Calendar getData() {
		return data;
	}

	public void setData(Calendar data) {
		this.data = data;
	}

	public Time getHorario() {
		return horario;
	}

	public void setHorario(Time horario) {
		this.horario = horario;
	}

	public String getInicio() {
		return inicio;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public String getFim() {
		return fim;
	}

	public void setFim(String fim) {
		this.fim = fim;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVerificador() {
		return verificador;
	}

	public void setVerificador(String verificador) {
		this.verificador = verificador;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public Contribuicao getContribuicao() {
		return contribuicao;
	}

	public void setContribuicao(Contribuicao contribuicao) {
		this.contribuicao = contribuicao;
	}
	
	
}
