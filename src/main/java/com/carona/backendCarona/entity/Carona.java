package com.carona.backendCarona.entity;

import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Carona {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="car_id") 
	private Long id;
	
	@Column(name="car_ponto_encontro") 
	private String ponto_encontro;
	
	@Column(name="car_acompanhantes") 
	private int acompanhantes;
	
	@Column(name="car_horario_aproximado") 
	private Time horario_aproximado;
	
	@Column(name="car_observacao") 
	private String observacao;
	
	@Column(name="car_situacao") 
	private String situacao;
	
	@ManyToOne
	@JoinColumn(name="car_fk_rot") 
	private Rota rota;
	
	@ManyToOne
	@JoinColumn(name="car_fk_usu") 
	private Usuario usuario;
	
	@ManyToOne
	@JoinColumn(name="car_fk_cont") 
	private Contribuicao contribuicao;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPonto_encontro() {
		return ponto_encontro;
	}

	public void setPonto_encontro(String ponto_encontro) {
		this.ponto_encontro = ponto_encontro;
	}

	public int getAcompanhantes() {
		return acompanhantes;
	}

	public void setAcompanhantes(int acompanhantes) {
		this.acompanhantes = acompanhantes;
	}

	public Time getHorario_aproximado() {
		return horario_aproximado;
	}

	public void setHorario_aproximado(Time horario_aproximado) {
		this.horario_aproximado = horario_aproximado;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

	public Rota getRota() {
		return rota;
	}

	public void setRota(Rota rota) {
		this.rota = rota;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Contribuicao getContribuicao() {
		return contribuicao;
	}

	public void setContribuicao(Contribuicao contribuicao) {
		this.contribuicao = contribuicao;
	}
	
	
}
