package com.carona.backendCarona.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carona.backendCarona.entity.Contribuicao;

@Repository
public interface ContribuicaoDao extends JpaRepository<Contribuicao, Long> {

}
