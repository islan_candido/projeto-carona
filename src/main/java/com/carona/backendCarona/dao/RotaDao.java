package com.carona.backendCarona.dao;

import java.util.Calendar;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carona.backendCarona.entity.Rota;

@Repository
public interface RotaDao extends JpaRepository<Rota, Long> {
	Rota findByVerificador(String verificador);
	List<Rota> findByStatusAndDataBetweenOrderByData(String status, Calendar data1, Calendar data2);
	List<Rota> findByStatusAndFimContainsIgnoreCaseAndDataBetweenOrderByData(String status, String fim, Calendar data1, Calendar data2);
	abstract boolean existsByVerificador(String verificador);
}
