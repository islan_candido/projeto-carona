package com.carona.backendCarona.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carona.backendCarona.entity.Veiculo;

@Repository
public interface VeiculoDao extends JpaRepository<Veiculo, Long>{
	Veiculo findByPlaca(String placa);
	abstract boolean existsByRenavam(String renavam);
	abstract boolean existsByPlaca(String placa);
}
