package com.carona.backendCarona.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carona.backendCarona.entity.Contato;

@Repository
public interface ContatoDao extends JpaRepository<Contato, Long>{
	public List<Contato> findByUsuarioCpf(String cpf);
}
