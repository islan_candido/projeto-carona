package com.carona.backendCarona.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carona.backendCarona.entity.Usuario;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, Long>{
	Usuario findByCpf(String cpf);
	abstract boolean existsByCpfAndSenha(String cpf, String senha);
	abstract boolean existsByCpf(String cpf);
	long countUsuarioFindByCpf(String cpf);
}
