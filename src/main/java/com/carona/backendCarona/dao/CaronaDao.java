package com.carona.backendCarona.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.carona.backendCarona.entity.Carona;

@Repository
public interface CaronaDao extends JpaRepository<Carona, Long>{
	Carona findByRotaVerificadorAndUsuarioCpf(String verificador, String cpf);
	List<Carona> findByRotaVeiculoUsuarioCpfAndSituacao(String cpf, String situacao);
	abstract boolean existsByRotaVerificadorAndUsuarioCpf(String verificador, String cpf);
}
